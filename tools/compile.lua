#!/usr/bin/lua
-- els to lua translator
--- (c) Er2 2021 <er2@dismail.de>
--- Zlib license

print '[1/4] Enter translation directory (without / at end)'
local dir = io.read()

local loc = {}

if not io.open(dir..'/LINGUAS') then
  print '[2/4] Enter languages divided by space'
  local l = io.read() .. ' '
  for v in l:gmatch '([%w_]+)%s+' do table.insert(loc, v) end
else
  print '[2/4] Reading LINGUAS'
  local f = io.open(dir..'/LINGUAS')
  for v in f:read('*a'):gmatch '([^\n]+)' do table.insert(loc, v) end
  f:close()
end

print '[3/4] Generating locales'
local t = os.time()
local lf = io.open(dir..'/loc.lua', 'w')
local k, v = 1
lf:write 'return{'
repeat v = loc[k]
  local f = io.open(dir..'/'..v..'.els')
  if f then print('  Generating', v)
    local tr = f:read '*a'
    f:close()
    lf:write(('%s={'):format(v))
    local trl, s = {}
    tr = tr..'\n\b'
    for l in tr:gmatch '([^\n]+)' do
      local str = l:match '^%s*"(.+)"%s*$'
      if l == '\b' or (str and str ~= s) then
        for _, v in pairs(trl) do
          v = v:gsub('\\(%$[%w_]+)', '\\\\%1')
          lf:write(('"%s",'):format(v))
        end
        if s then lf:write '},' end
        trl, s = {str}, str
        if s then
          print('   |> String', s)
          lf:write(('["%s"]={'):format(s))
        end
      end

      str = l:match '^%s*=%s*"(.+)"%s*$'
      if str and s then
        print('    =>', str)
        trl[1] = str
      end
      str = l:match '^%s*>%s*"(.+)"%s*$'
      if str and s then
        print('   ||>', str)
        table.insert(trl, str)
      end
    end

    lf:write '},'
  else
    print('  404 Not found', v)
    table.remove(loc, k)
    k = k - 1
  end
  k = k + 1
  v = loc[k]
until not v
lf:write '}'
lf:flush()
lf:close()
print(('%d seconds'):format(os.time() - t))

print '[4/4] Locale linting'
require(dir..'.loc')

print 'Done!'

#!/usr/bin/lua
-- po to els translator
--- (c) Er2 2021 <er2@dismail.de>
--- Zlib license

print '[1/3] Enter translation directory (without / at end)'
local dir = io.read()

local loc = {}

if not io.open(dir..'/LINGUAS') then
  print '[2/3] Enter languages divided by space'
  local l = io.read() .. ' '
  for v in l:gmatch '([%w_]+)%s+' do table.insert(loc, v) end
else
  print '[2/3] Reading LINGUAS'
  local f = io.open(dir..'/LINGUAS')
  for v in f:read('*a'):gmatch '([^\n]+)' do table.insert(loc, v) end
  f:close()
end

print '[3/3] Generating els'
local t = os.time()
local w = 0
local k, v = 1
repeat v = loc[k]
  local f = io.open(dir..'/'..v..'.po')
  if f then print(' Generating', v)
    local tr = f:read '*a'
    f:close()
    f = io.open(dir..'/'..v..'.els', 'w')
    f:write 'Er2 Locale source file\n'
    for l in tr:gmatch '[^\n]+' do
      if l:match '%%%a' then w = w + 1
        print('  WARN', 'Found printf string, not will be replaced')
        print('  INFO', l)
      end
      l = l
      :gsub('^%s*".*"%s*$', '')
      :gsub('^msgid_plural.+', '')
      :gsub('^msgid%s+"(.*)"', '"%1"')
      :gsub('^msgstr%s+"(.*)"', ' = "%1"')
      :gsub('^msgstr%s*%[%s*(%d+)%s*%]%s+(".*")', function(id, str)
        return (id == '0' and ' = ' or ' > ') .. str end)
      :gsub('^%s*[=>]?%s*""', '')
      if #l > 0 then f:write(l, '\n') end
    end
    f:flush()
    f:close()
  else
    print(' 404 Not found', v)
    table.remove(loc, k)
    k = k - 1
  end
  k = k + 1
  v = loc[k]
until not v
print(('%d seconds, %d warnings'):format(os.time() - t, w))

print 'Done!'

-- Improved gettext
--- Er2 Locale
--- (c) Er2 2021 <er2@dismail.de>
--- Zlib License
local gtext = {
  dloc = 'en_US', sdloc = 'en',
  pref = '$'
}
function gtext:__call(str, t)
  t = type(t) == 'table' and t or {}
  t.plural = t.plural or 0
  local plf = t.plural + 1
  local tr = self.trans[self.locale or self.floc]
  if tr and tr[str] and tr[str][plf] then str = tr[str][plf]
  else
    tr = self.trans[self.floc]
    if tr and tr[str] and tr[str][plf] then str = tr[str][plf] end
  end

  -- Interpolation
  str = str:gsub('(.?)%'..gtext.pref..'([%w_]+)', function(p, v)
    if p == '\\' then return gtext.pref..v end
    return p.. tostring(t[v] or '')
  end)
  return str
end

return function(path)
  local self = setmetatable({
    floc = gtext.dloc,
    trans = require(path..'.loc')
  }, gtext)

  if not self.trans[self.floc] then self.floc = gtext.sdloc end
  if not self.trans[self.floc] then self.floc = l:match '([^\n]+)' end
  self.locale = self.floc
  _, gettext = self, self
end
